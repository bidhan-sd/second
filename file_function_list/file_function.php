<?php
/*
    File function list:
    1.basename
    2.copy
    3.fclose
    4.fgetcsv
    5.file_exist
    6.file_get_content
    7.file_put_content
    8.filesize
    9.dirname
    10.filetype
    11.Fopen
    12.fputcsv
    13.fwrite
    14.is_file
    15.move_upload_file
    16.unlink
    //Class hocha collection of method and properties is called class.

 */

$handle = fopen('wfile.txt', 'w');
$content = 'Hello this is a test string writing to file "wfile.txt"';
// Write $content to opened file.
if (fwrite($handle, $content) === FALSE) {
    echo 'Cannot write to file';
    return;
} else {
        echo 'written';
}
