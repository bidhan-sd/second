<?php

class Calculator {
    public $_val1 , $_val2;

    public function __construct($val1, $val2){
        $this->_val1 = $val1;
        $this->_val2 = $val2;
    }

    public function add(){
        return $this->_val1 + $this->_val2;
    }

    public function subtract(){
        return $this->_val1 - $this->_val2;
    }

    public function multiply (){
        return $this->_val1 * $this->_val2;
    }

    public function divide () {
        return $this->_val1 / $this->_val2;
    }
}
if(isset($_POST['submit'])){
    $calc = new Calculator($_POST['num1'],$_POST['num2']);
    echo "<p>".$calc->add(). "</p>";

    $calc = new Calculator ($_POST['num1'],$_POST['num2']);
    echo "<p>".$calc->subtract(). "</p>";

    $calc = new Calculator ($_POST['num1'],$_POST['num2']);
    echo "<p>".$calc->multiply(). "</p>";

    $calc = new Calculator ($_POST['num1'],$_POST['num2']);
    echo "<p>".$calc ->divide(). "</p>";
}

?>

<form method="POST" action="">
    <table>
        <tr>
            <td>Number1 :</td>
        </tr>
        <tr>
            <td><input type="text" name="num1" value=""/></td>
        </tr>
        <tr>
            <td>Number2 :</td>
        </tr>
        <tr>
            <td><input type="text" name="num2" value=""/></td>
        </tr>
        <tr>
            <td></td>
        </tr>
        <tr>
            <td>
                <input type="submit" name="submit" value="Calculate"/>
            </td>
        </tr>
    </table>
</form>
