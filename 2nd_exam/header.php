 <?php 
$bgcolor = "#ccc";
$hdcolor = "#777";
$fsize = "red";
?>
<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<title>PHP syntax</title>
	<link rel="stylesheet" href="style.css" />
	<style type="text/css">
		.phpcoding{width:900px;margin:0 auto;background:<?php echo $bgcolor;?>;}
		.headeroption {background:<?php echo $hdcolor ; ?>;color:#fff;text-align:center; min-height: 70px;}
		.headeroption  h1{font-size: 38px;margin: 0px;padding-top: 10px;}
		.footeroption {background:#444;color:<?php echo $fsize ; ?>;text-align:center; min-height: 60px;}
		.footeroption  p{font-size: 25px;margin: 0px;padding-top: 17px;color: #fff;}
		.maincontent{min-height:400px;padding:20px;}	
		#myform{width:400px;border:1px solid #fff;padding:10px;}
		.tblone{width:420px;border:1px solid #fff;margin:20px 0px;}
		.tblone tr:nth-child(2n+1){background:#ddd;height: 30px}
		.tblone tr:nth-child(2n){background:#f1f1f1;height: 30px}
		.tblone td{padding:5px 10px;}
	</style>
		<link rel="stylesheet" href="inc/bootstrap.min.css">
		<script src="inc/jquery.min.js"></script>
		<script src="inc/bootstrap.min.js"></script>
</head>
<body>
	<section class="phpcoding">
		<section class="headeroption">
			<h1>PHP fundamantal traning</h1>
		</section>
		<section class="maincontent">
			 <h1 align="center" style="margin:0">All input type here </h1><hr>