<?php
/*
	Different type of data in php list:

	Integer,
	Dobule,
	String,
	Boolean,
	Array(),
	//Special type
	NULL,
	Object,
	Resource,



/*
	Variable handleing function list by suman ahmed.
	1.boolval()
	2.empty()
	3.gettype()
	4.is_array()
	5.is_int()
	6.is_null()
	7.isset()
	8.print_r()
	9.serialize()
	10.unserialize()
	11.unset()
	12.var_dump()
*/

/*$var = array("100.00251");
echo is_array($var);

$var = 1;
echo is_int($var);

$var = 1.0;
echo is_float($var);

$var = null;
echo is_null($var);


$var = "bidhan";
echo isset($var);


$var = array("Bidhan","sutradhar","dhaka","kanchanpur");
echo "<pre>";
print_r($var);

$var = array("Bidhan","sutradhar","dhaka","kanchanpur");
echo "<pre>";
var_dump($var);

$var = "bidhan";
echo $var;
unset($var);
echo $var;


$name = array(
	"B" => "bangladesh",
	"I" => "India",
	"S" => "Sri-lanka",
	"E" => "England",
	"A" => "Australia",
	"S" => "South-Africa",
);
echo "<pre>";
print_r($name);

echo "<br/><br/><br/>";

$serialize = serialize($name);
print_r($serialize);

echo "<br/>";
echo "<br/>";
echo "<br/>";

echo "After unserialize data.";

$orginal_array = unserialize($serialize);

print_r($orginal_array);



$str = <<<bidhan
This is heredoc string type example
bidhan;
echo $str;

echo "<br/>";

$str = <<<'bidhan'
This is nowdoc string type example
bidhan;
echo $str;


//Constant type data example bellow.

define("variable","Bidhan sutradhar");
echo variable;


//Magic method here.
echo __LINE__;
echo __FILE__;
echo __DIR__;

class Bidhan{
	
	function __construct()
	{
		echo "The class name is :".__CLASS__;
	}
}
$obj = new Bidhan();


function bidhanSutradhar(){
	echo "The Function name is :".__FUNCTION__;
}
bidhanSutradhar();



function myMethod(){
	echo "The Method name is :".__METHOD__;
}
myMethod();

*/
namespace MySampleNS;
echo "the namespace is: " . __NAMESPACE__;
