<?php

class Cricket{
	public function __construct(){
		echo "This is cricket class constructor output.<br/>";
	}
}

class Movie extends Cricket{
	public function __construct(){
		parent::__construct();
		//new Cricket();
		echo "This is Movie class constructor outpur here.";
	}
}
$obj = new Movie();