<form action="calculation.php" method="POST">
    <table>
        <tr>
            <td>Math :<input type="text" name="math" value=""/></td>
        </tr>
        <tr>
            <td>Eng :<input type="text" name="eng" value=""/></td>
        </tr>
        <tr>
            <td><input name="submit" type="submit" value="Calculate"/></td>
        </tr>
    </table>
</form>


<?php
$num1 = $_POST['math'];
$num2 = $_POST['eng'];
$calcu = $_POST['submit'];

function calculate($n1,$n2, $calcu) // set $calcu as parameter
{
    switch($calcu)
    {
        case "Addition": // here you have to use colons not semi-colons
            $compute = $n1 + $n2;
            break;
        case "Subtraction":
            $compute = $n1 - $n2;
            break;
        case "Multiplication":
            $compute = $n1 * $n2;
            break;
        case "Division":
            $compute = $n1 / $n2;
            break;
    }
    return $compute; // returning variable
}
echo "$calcu <br /> <br /> 1st Number: $num1 <br /> 2nd Number: $num2 <br /><br />";
echo "Answer is:" .calculate($num1,$num2, $calcu); // you need to pass $calcu as argument of that function
?>

