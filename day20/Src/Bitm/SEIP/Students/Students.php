<?php
namespace App\Bitm\SEIP\Students;

use pdo;


class Students
{
    public $name;
    public $id;
    public $item;
    public function setData($data = '')
    {
    if(array_key_exists('title',$data))
    {
        $this->name = $data['title'];
    }
        if(array_key_exists('id',$data))
        {
            $this->id = $data['id'];
        }
        return $this;
    }
    public function index()
    {
        try {
            $pdo = new pdo('mysql:host=localhost;dbname=studentdb', 'root', '');
            $query = "SELECT * FROM `student`";

            $stmt = $pdo->prepare($query);

            $stmt->execute();

            $data = $stmt->fetchAll();
            return $data;
        } catch (PDOException $e) {
            echo "Error" . $e->getMessage();
        }
    }

    public function Store()
    {
        try {
            $pdo = new pdo('mysql:host=localhost;dbname=studentdb', 'root', '');
            $query = "INSERT INTO `student` (`id`, `title`) VALUES (:a,:b)";

            $stmt = $pdo->prepare($query);

            $stmt->execute(
                array(
                    ':a' => 'null',
                    ':b' => $this->name

                )
            );
            if ($stmt) {
                session_start();
                header("location:create.php");
                $_SESSION['message'] = "Data Submitted successfully";
            }

        } catch (PDOException $e) {
            echo "Error" . $e->getMessage();
        }

    }
    public function show()
    {
        try {
            $pdo = new pdo('mysql:host=localhost;dbname=studentdb', 'root', '');
            $query = "SELECT * FROM `student` WHERE id=$this->id";

            $stmt = $pdo->prepare($query);

            $stmt->execute();

            $data = $stmt->fetch();
            return $data;
        } catch (PDOException $e) {
            echo "Error" . $e->getMessage();
        }
    }
    public function update()
    {
        try {
            $pdo = new pdo('mysql:host=localhost;dbname=studentdb', 'root', '');
            $query = "UPDATE student SET title = :title WHERE id=:id";
            $stmt = $pdo->prepare($query);
            $stmt->execute(array(
                ':id'=>$this->id,
                ':title'=>$this->name,
            ));
            if($stmt){
                session_start();
                $_SESSION['message']="Successfullay updated";
                header('location:index.php');
            }
        } catch (PDOException $e) {
            echo "Error" . $e->getMessage();
        }
    }
    public function search($searchItem=''){
        $this->item=$searchItem['search'];
        try {

            $pdo = new pdo('mysql:host=localhost;dbname=studentdb', 'root', '');
            $query = "SELECT * FROM `student` WHERE `title`= '$this->item'";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetch();
            return $data;
        } catch (PDOException $e) {
            echo "Error" . $e->getMessage();
        }
    }

}

