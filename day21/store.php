<?php
include_once "Student.php";

if(empty($_POST['title'])){
    session_start();
    $_SESSION['message'] = "Name field can't be empty.";
    header('location: create.php');
}else{
    $obj = new Student();
    $obj->setData($_POST);
    $obj->store();
}
