<?php
use pdo;
class Student{
    public $id='';
    public $title='';
    public $password='';
    public $sex='';
    public $groups='';
    public $years='';

    public function setData($data=''){
        if(array_key_exists('title',$data)){
            $this->title=$data['title'];
        }
        if(array_key_exists('password',$data)){
            $this->title=$data['password'];
        }
        if(array_key_exists('sex',$data)){
            $this->title=$data['sex'];
        }
        if(array_key_exists('group',$data)){
            $this->title=$data['group'];
        }
        if(array_key_exists('pass_year',$data)){
            $this->title=$data['pass_year'];
        }
        return $this;
    }
    public function store(){
        try {
            $pdo = new pdo('mysql:host=localhost;dbname=studentdb', 'root', '');
            $query = "INSERT INTO `student` (`id`, `title`,`password`,`sex`,`groups`,`year`,) VALUES (:id,:title,:password,:sex,:groups,:years)";

            $stmt = $pdo->prepare($query);

            $stmt->execute(
                array(
                    ':id' => 'null',
                    ':title' => $this->title,
                    ':password' => $this->password,
                    ':sex' => $this->sex,
                    ':groups' => $this->groups,
                    ':years' => $this->years,

                )
            );
            if ($stmt) {
                session_start();
                header("location:create.php");
                $_SESSION['message'] = "Data Submitted successfully";
            }

        } catch (PDOException $e) {
            echo "Error" . $e->getMessage();
        }
    }
}