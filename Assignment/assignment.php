<?php
/*
    email:bitm.training@gmail.com
    Subject : include() vs include_once() and required() vs required_once()

*/
class Dada{
    public function __construct(){
        echo "Bidhan sutradhar <br/>";
    }
}
class Baba extends Dada{

    public function __construct()
    {
        parent::__construct();
        echo "Apoun mix";
    }
}
$obj = new Baba();

