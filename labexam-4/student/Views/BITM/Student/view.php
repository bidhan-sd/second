<?php
include_once("../../../vendor/autoload.php");
use App\BITM\Student\Student;
$obj = new Student();
$alldata = $obj->viewAll();

?>
<table>
    <tr>
        <th width="10%">Serial</th><th width="50%">Name</th><th width="40%">Action</th>
    </tr>

    <?php
    $i=0;
    foreach($alldata as $key=>$value){
        $i++;
    ?>
    <tr>
        <td><?php echo $i; ?></td>
        <td><?php echo $value['title']; ?></td>
        <td>
            <a href="single.php?id=<?php echo $value['id'];?>">Edit</a>/
            <a onsubmit="confirm('Are you sere delete this data')" href="delete.php?id=<?php echo $value['id']; ?>">Delete</a>
        </td>
    </tr>
    <?php } ?>
</table>