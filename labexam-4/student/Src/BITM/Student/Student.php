<?php
namespace App\BITM\Student;
use PDO;
class Student{
	public $name = '';
	public $password = '';
	public $email = '';
	public $gender = '';
	public $delete_id='';
	public $single_id = '';
	public function Create($data = ''){
		if(array_key_exists('title',$data)){
			$this->name = $data['title'];
		}else{
			echo "Name field can't be empty.";
		}

		if(array_key_exists('password',$data)){
			$this->password = $data['password'];
		}else{
			echo "Password field can't be empty.";
		}

		if(array_key_exists('email',$data)){
			$this->email= $data['email'];
		}else{
			echo "Email field can't be empty.";
		}

		if(array_key_exists('sex',$data)){
			$this->gender= $data['sex'];
		}else{
			echo "Gender field can't be empty.";
		}
	}
	public function getData(){

		try {
			$pdo = new PDO('mysql:host=localhost;dbname=Studentdb', 'root', '');
			$stmt = $pdo->prepare('INSERT INTO `Students` (`title`, `password`, `email`, `gender`) VALUES (:title,:password,:email,:gender)');

			$stmt->execute(array(
					':title' => $this->name,
					':password' => $this->password,
					':email' => $this->email,
					':gender' => $this->gender,
			));
			if($stmt){
				session_start();
				$_SESSION['message'] = "Data inserted successfully."
//				header("location :create.php");
			}

		} catch(PDOException $e) {
			echo 'Error: ' . $e->getMessage();
		}

	}
	public function viewAll(){
		try {
			$pdo = new PDO('mysql:host=localhost;dbname=Studentdb', 'root', '');
			$stmt = $pdo->prepare('SELECT * FROM students');
			$stmt->execute(array());
			$stmt->fetch();
			return $stmt;
		} catch(PDOException $e) {
			echo 'Error: ' . $e->getMessage();
		}
	}
	public function deleteData($deletdid = ''){
		$this->delete_id = $deletdid['id'];
		try {
			$pdo = new PDO('mysql:host=localhost;dbname=Studentdb', 'root', '');
			$stmt = $pdo->prepare('DELETE FROM students WHERE id = :id');
			$stmt->bindParam(':id',$this->delete_id = $deletdid['id']);
			$stmt->execute();
			return $stmt;
		} catch(PDOException $e) {
			echo 'Error: ' . $e->getMessage();
		}
	}
	public function singleIDshow($sin_id=''){
		$this->single_id = $sin_id['id'];
	}
	public function SingleData(){

		try {
			$pdo = new PDO('mysql:host=localhost;dbname=Studentdb', 'root', '');
			$stmt = $pdo->prepare('Select * FROM students WHERE id=?');
			$stmt->execute(array($this->single_id));
			$stmt->fetch();
			return $stmt;
		} catch(PDOException $e) {
			echo 'Error: ' . $e->getMessage();
		}
	}

}
